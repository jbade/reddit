const presets = [{
    subreddits: [
        'SkyPorn', 'EarthPorn'
    ],
    link: 'Earth',
    label: 'great views'
}, {
    subreddits: ['spaceporn'],
    link: 'Space',
    label: 'distant galaxies'
}, {
    subreddits: [
        'AnimalPorn', 'BotanicalPorn', 'wildlifephotography'
    ],
    link: 'Nature',
    label: 'plants and animals'
}, {
    subreddits: [
        'aww', 'cats'
    ],
    link: 'Aww',
    label: 'cute animals'
}, {
    subreddits: [
        'food', 'GifRecipes', 'DessertPorn', 'FoodPorn', 'CulinaryPorn'
    ],
    link: 'Food',
    label: 'don\'t get hungry'
}, {
    subreddits: [
        'ExposurePorn', 'MacroPorn', 'itookapicture', 'photocritique'
    ],
    link: 'Photography',
    label: 'camera art'
}, {
    subreddits: ['CarPorn'],
    link: 'Cars',
    label: 'motor vehicles'
}, {
    subreddits: [
        'ArtPorn', 'art'
    ],
    link: 'Art',
    label: 'visual arts'
},{
    subreddits: [
        'mildlyinteresting', 'interestingasfuck', 'oddlysatisfying', 'mildlyinfuriating'
    ],
    link: 'Interesting',
    label: 'interesting, satisfying, and infuriating images'
}, {
    subreddits: [
        'pics', 'funny', 'gifs'
    ],
    link: 'Pics',
    label: 'pics and gifs'
}, {
    subreddits: [
        'wtf', 'creepy'
    ],
    link: 'Wtf',
    label: 'weird and creepy'
}, {
    subreddits: [
        'OldSchoolCool', 'TheWayWeWere'
    ],
    link: 'Old school',
    label: 'the way we were'
}, {
    subreddits: ['diy'],
    link: 'DIY',
    label: 'I made it'
}, {
    subreddits: ['all'],
    link: 'All',
    label: 'everything'
}];

export {
    presets
};
